package com.adam8234.recyclerushscorer;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * @author adam
 * @since 1/4/15.
 */
public class NumberWidget extends FrameLayout {
    private int valuePerNumber;
    private TextView textView;
    private EditText editText;

    public NumberWidget(Context context) {
        super(context);
        init((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
    }

    public NumberWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
    }

    public NumberWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
    }

    private void init(LayoutInflater inflater){
        inflater.inflate(R.layout.number_widget, this);
        textView = (TextView) findViewById(R.id.title);
        editText = (EditText) findViewById(R.id.number_edit);
        editText.setText("0");
    }

    public void setValuePerNumber(int number){
        valuePerNumber = number;
    }

    public int getValue(){
        if(editText.getText().toString().isEmpty()){
            return 0;
        }
        return Integer.parseInt(editText.getText().toString());
    }

    public int getValuePerNumber() {
        return valuePerNumber;
    }

    public void setTitle(String title){
        textView.setText(title);
    }

    public void setTextChangedListener(TextWatcher textChangedListener){
        editText.addTextChangedListener(textChangedListener);
    }



}
