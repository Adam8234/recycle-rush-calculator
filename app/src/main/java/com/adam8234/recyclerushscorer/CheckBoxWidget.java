package com.adam8234.recyclerushscorer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * @author adam
 * @since 1/4/15.
 */
public class CheckBoxWidget extends FrameLayout {
    private TextView titleView;
    private CheckBox checkBox;
    private int value;

    public CheckBoxWidget(Context context) {
        super(context);
        init((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE));

    }

    public CheckBoxWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
    }

    public CheckBoxWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
    }

    private void init(LayoutInflater inflater){
        inflater.inflate(R.layout.checkbox_widget, this);
        titleView = (TextView) findViewById(R.id.title);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
    }

    public void setCheckBox(boolean set){
        checkBox.setChecked(set);
    }

    public boolean isChecked(){
        return checkBox.isChecked();
    }

    public int getValue(){
        return value;
    }

    public void setValue(int value){
        this.value = value;
    }

    public void setTitleView(String string){
        titleView.setText(string);
    }

}
