package com.adam8234.recyclerushscorer;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements View.OnClickListener, TextWatcher {


    TextAppearanceSpan textAppearanceSpan;
    private ArrayList<NumberWidget> mTeleScoringOptions;
    private ArrayList<CheckBoxWidget> mAutoScoreOptions, mCoopScoreOptions;
    private LinearLayout teleLinearLayout, coopLinearLayout, autoLinearLayout;
    private TextView teleScoreText, autoScoreText, totalScoreText;
    private int currentCoopScore, currentTeleScore, currentAutoScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textAppearanceSpan = new TextAppearanceSpan(this, R.style.TextAppearanceScore);
        totalScoreText = (TextView) findViewById(R.id.total_score);
        initAutoViews();
        initTeleopViews();
        initCoopViews();
    }

    void initAutoViews() {
        mAutoScoreOptions = new ArrayList<>();
        autoScoreText = (TextView) findViewById(R.id.auto_score);
        autoLinearLayout = (LinearLayout) findViewById(R.id.auto_list_view);

        CheckBoxWidget robotSet = new CheckBoxWidget(this);
        robotSet.setTitleView("Robot Set");
        robotSet.setValue(4);
        robotSet.setOnClickListener(this);
        autoLinearLayout.addView(robotSet);
        mAutoScoreOptions.add(robotSet);

        CheckBoxWidget toteSet = new CheckBoxWidget(this);
        toteSet.setTitleView("Tote Set");
        toteSet.setValue(6);
        toteSet.setOnClickListener(this);
        autoLinearLayout.addView(toteSet);
        mAutoScoreOptions.add(toteSet);

        CheckBoxWidget containerSet = new CheckBoxWidget(this);
        containerSet.setValue(8);
        containerSet.setTitleView("Container Set");
        containerSet.setOnClickListener(this);
        autoLinearLayout.addView(containerSet);
        mAutoScoreOptions.add(containerSet);

        CheckBoxWidget stackedToteSet = new CheckBoxWidget(this);
        stackedToteSet.setValue(20);
        stackedToteSet.setTitleView("Stacked Tote Set");
        stackedToteSet.setOnClickListener(this);
        autoLinearLayout.addView(stackedToteSet);
        mAutoScoreOptions.add(stackedToteSet);

        updateAutoScore();
    }

    void initTeleopViews() {
        teleLinearLayout = (LinearLayout) findViewById(R.id.tele_list_view);
        teleScoreText = (TextView) findViewById(R.id.tele_score);
        mTeleScoringOptions = new ArrayList<>();

        NumberWidget scoredGrayTotes = new NumberWidget(this);
        scoredGrayTotes.setTitle("Scored Gray Totes");
        scoredGrayTotes.setValuePerNumber(2);
        scoredGrayTotes.setTextChangedListener(this);
        teleLinearLayout.addView(scoredGrayTotes);
        mTeleScoringOptions.add(scoredGrayTotes);

        NumberWidget sumOfRC = new NumberWidget(this);
        sumOfRC.setTitle("Sum of totes under all Recycling Containers");
        sumOfRC.setValuePerNumber(4);
        sumOfRC.setTextChangedListener(this);
        teleLinearLayout.addView(sumOfRC);
        mTeleScoringOptions.add(sumOfRC);

        NumberWidget litterInRc = new NumberWidget(this);
        litterInRc.setTitle("Number of Litter on/in Recycling Container");
        litterInRc.setValuePerNumber(6);
        litterInRc.setTextChangedListener(this);
        teleLinearLayout.addView(litterInRc);
        mTeleScoringOptions.add(litterInRc);

        NumberWidget litterInLandfill = new NumberWidget(this);
        litterInLandfill.setTitle("Litter in Landfill");
        litterInLandfill.setValuePerNumber(1);
        litterInLandfill.setTextChangedListener(this);
        teleLinearLayout.addView(litterInLandfill);
        mTeleScoringOptions.add(litterInLandfill);

        NumberWidget unprocessedLitterBonus = new NumberWidget(this);
        unprocessedLitterBonus.setTitle("Unprocessed Litter Bonus");
        unprocessedLitterBonus.setValuePerNumber(4);
        unprocessedLitterBonus.setTextChangedListener(this);
        teleLinearLayout.addView(unprocessedLitterBonus);
        mTeleScoringOptions.add(unprocessedLitterBonus);

        updateTeleScore();

    }

    void initCoopViews() {
        mCoopScoreOptions = new ArrayList<CheckBoxWidget>();
        coopLinearLayout = (LinearLayout) findViewById(R.id.coop_list_view);

        CheckBoxWidget robotSet = new CheckBoxWidget(this);
        robotSet.setTitleView("Coopertition Set");
        robotSet.setValue(20);
        robotSet.setOnClickListener(this);
        coopLinearLayout.addView(robotSet);
        mCoopScoreOptions.add(robotSet);

        CheckBoxWidget toteSet = new CheckBoxWidget(this);
        toteSet.setTitleView("Coopertition Stack");
        toteSet.setValue(40);
        toteSet.setOnClickListener(this);
        coopLinearLayout.addView(toteSet);
        mCoopScoreOptions.add(toteSet);

        updateCoopScore();
    }

    private void updateTotalScore() {
        int score = currentAutoScore + currentTeleScore;
        SpannableString string = new SpannableString("Total Score: " + score);
        string.setSpan(textAppearanceSpan, "Total Score:".length(), string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        totalScoreText.setText(string);
    }

    @Override
    public void onClick(View v) {
        if (v instanceof CheckBoxWidget) {
            CheckBoxWidget checkBoxWidget = (CheckBoxWidget) v;
            (checkBoxWidget).setCheckBox(!(checkBoxWidget.isChecked()));
            if (checkBoxWidget == mCoopScoreOptions.get(1)) {
                mCoopScoreOptions.get(0).setCheckBox(false);
            } else if (checkBoxWidget == mCoopScoreOptions.get(0)) {
                mCoopScoreOptions.get(1).setCheckBox(false);
            }
        }
        updateCoopScore();
        updateAutoScore();
    }

    void updateCoopScore() {
        int score = 0;

        for (CheckBoxWidget checkBoxWidget : mCoopScoreOptions) {
            score += checkBoxWidget.isChecked() ? checkBoxWidget.getValue() : 0;
        }

        currentCoopScore = score;
        updateTeleScore();
    }

    private void updateAutoScore() {
        int score = 0;

        for (CheckBoxWidget checkBoxWidget : mAutoScoreOptions) {
            score += checkBoxWidget.isChecked() ? checkBoxWidget.getValue() : 0;
        }

        currentAutoScore = score;
        SpannableString string = new SpannableString("Auto: " + currentAutoScore);
        string.setSpan(textAppearanceSpan, "Auto:".length(), string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        autoScoreText.setText(string);
        updateTotalScore();
    }

    private void updateTeleScore() {
        int score = 0;

        for (NumberWidget numberWidget : mTeleScoringOptions) {
            score += numberWidget.getValue() * numberWidget.getValuePerNumber();
        }

        currentTeleScore = score + currentCoopScore;
        SpannableString string = new SpannableString("Tele: " + currentTeleScore);
        string.setSpan(textAppearanceSpan, "Tele:".length(), string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        teleScoreText.setText(string);
        updateTotalScore();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        updateTeleScore();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
